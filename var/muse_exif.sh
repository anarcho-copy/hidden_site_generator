#!/bin/bash

default_lang="tr"

function guide() {
cat <<EOF
usage: ./`basename $0` -f FILENAME [-l LANG -s STATUS]
EOF
exit 0
}

while getopts ":f:l:s:" opt; do
  case ${opt} in
    f ) file=${OPTARG} ;;
    l ) lang=${OPTARG} ;;
    s ) status=${OPTARG} ;;
    : )
      echo "Missing option argument for -$OPTARG"
      exit 0
      ;;
  esac
done

if [ "$#" -lt 1 ]; then
    guide
fi

if [ -z "$file" ]
 then
   echo "file name not specified."
   echo "aborting"
   exit 0
 else
   if [ ! -f "$file" ]
     then
       echo "file not found."
       echo "aborting"
       exit 0
     else
	type="library"
	date=$(date '+%Y:%m:%d %T')
	id=$(cat /proc/sys/kernel/random/uuid)
	source=$(echo "$file" | cut -f 1 -d '.')
	subtitle=$(exiftool -s -s -s -Subject "$file")
	[ -z "$lang" ] && lang="$default_lang"
	echo "DATE	: $date"
	echo "ID	: $id"
	echo "SOURCE	: $source"
	echo "LANG	: $lang"
	echo "STATUS	: $status"
	echo "TYPE	: $type"
	echo "SUBTITLE	: $subtitle"
       exiftool -m \
        -DocumentID="uuid:$id" \
        -Source="$source" \
        -Status="$status" \
	-Date="$date" \
        -Language="$lang" \
	-Type="$type" \
	-State="$subtitle" "$file"
   fi
fi
