#!/bin/bash
slug="../src/slug_v0.2.2.sh"

count=$(sqlite3 books.db "SELECT COUNT(*) FROM books;");
for i in `seq 1 $count`;
do
    author=`sqlite3 books.db "SELECT author FROM books WHERE rowid='$i'"`;
    _author="$($slug "$author")"
    sqlite3 books.db "UPDATE books SET slug='$_author' WHERE rowid='$i'";
    echo "$author  >  $_author       : $i";
done
