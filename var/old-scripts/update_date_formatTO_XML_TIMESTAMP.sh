#!/bin/bash
#for exiftool compaitable.
#from date '+%Y:%m:%d %T'
#to date '+%a, %d %b %Y %H:%M:%S GMT'	#XML TIMESTAMP

count=$(sqlite3 books.db "SELECT COUNT(*) FROM books;");
for i in `seq 1 $count`;
do
    date=`sqlite3 books.db "SELECT date FROM books WHERE rowid='$i'"`;
    _year=$(echo "$date" | awk '{print $1}')
    hour=$(echo "$date" | awk '{print $2}')
    year=$(echo "$_year" | sed 's/:/\//g')
    xml_date=$(date -d "$year $hour" +"%a, %d %b %Y %H:%M:%S GMT")
    sqlite3 books.db "UPDATE books SET date='$xml_date' WHERE rowid='$i'";
    echo "$date  >  $xml_date       : $i";
done
