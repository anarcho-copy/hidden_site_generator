#!/bin/bash
db="books.db";

function guide() {
cat <<EOF
usage: `basename $0` [ -i <uuid> [-s] [-g] ] [-l]
-s    enter new html to uuid
-g    get html code from uuid
-l    list uuids
EOF
exit 0
}

function save_to_db() {
[[ -z $id ]] && echo "id not gived, aborting." && exit 0
echo -e "enter html lines\nMissing [CTRL+D] for exit from stdin\n"
code=$(</dev/stdin)
sqlite3 $db "DELETE FROM article_html WHERE id IS '$id';"
sqlite3 $db "INSERT OR REPLACE INTO article_html (id,code) VALUES ('$id','`echo $code`');"
exit 0
}

function get_code() {
[[ -z $id ]] && echo "id not gived, aborting." && exit 0
sqlite3 $db "SELECT code FROM article_html WHERE id IS '$id';"
exit 0
}

while getopts ":i:gsl" opt; do
  case ${opt} in
    i ) id=${OPTARG} ;;
    g ) get_code ;;
    s ) save_to_db ;;
    l ) sqlite3 $db "SELECT id FROM article_html;" ;;
    : ) echo "Missing option argument for -$OPTARG" ; exit 0 ;;
  esac
done

if [ "$#" -lt 1 ]; then
    guide
fi
