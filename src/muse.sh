#!/bin/bash
#The Anarchist Library Friendly

#CONFIG
link="https://anarcho-copy.org"
TIMESTAMP=$(date -u +%Y-%m-%dT%T)
cd "$(dirname "$0")"
cd ../bin
. config.sh #import
cd - &> /dev/null
db_file="../var/books.db"
index_chars=$(sqlite3 $db_file "SELECT author FROM books" | cut -c 1 | LC_ALL=tr_TR.UTF-8 tr '[:lower:]' '[:upper:]' | sort -u | tr '\n' ' ')


function author_table() {
sqlite3 $db_file "SELECT title,url FROM books WHERE author IS '$1'" > .table.txt
while IFS="|" read -r title url
do
cat <<EOT

		- [[$link/copy/$url][$title]]

EOT
done < .table.txt
}


function start_index() {
sqlite3 $db_file "SELECT author FROM books WHERE author LIKE '$1%'" > .list.txt
sort .list.txt | uniq -ci > .listed.txt
while IFS=" " read -r count author
do
id=$(echo "$author" | bash $url_slug)
cat <<EOT

	- $author <sup>$count</sup>
$(author_table "$author")
EOT
done < .listed.txt
}


function start_loop() {
for x in $index_chars
do
cat <<EOT

#anchor-$x
 - $x
$(start_index $x)
EOT
done
}


function letters() {
for x in $index_chars
do
cat <<EOT
- [[#anchor-$x][**$x**]]
EOT
done
}


# MAIN
cat <<EOT | sed 's/-Ü/-UE/g; s/-Ç/-CH/g; s/-İ/-IE/g; s/-Ğ/-GH/g; s/-Ş/-SH/g'
#title Kitaplar
#lang tr
#pubdate $TIMESTAMP
#notes Son güncelleme: $TIMESTAMP

<center>
$(letters)
-
</center>
<br>
$(start_loop)
EOT


rm .table.txt .list.txt .listed.txt &>/dev/null
